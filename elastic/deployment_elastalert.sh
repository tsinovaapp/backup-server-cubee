sudo docker stop $(sudo docker ps -q --filter ancestor=bitsensor/elastalert:3.0.0-beta.0)
sudo docker rm $(sudo docker ps -a -q --filter ancestor=bitsensor/elastalert:3.0.0-beta.0)
sudo docker-compose -f docker-compose-elastalert.yml up -d
sudo chmod 777 -Rf /var/lib/docker/volumes/elastic_elastalert_rules/
